package br.com.javaparaweb.financeiro.conta;

import java.util.List;

import br.com.javaparaweb.financeiro.usuario.Usuario;

/**
 * Interface da classe {@code Conta} com m�todos a serem implementados
 * @author Filipe Lobato
 *
 */
public interface ContaDAO {
	/**
	 * Salva conta utilizando o Hibernate.
	 * @param conta A entidade conta
	 */
	public void salvar(Conta conta);
	
	/**
	 * Exclue conta utilizando o Hibernate.
	 * @param conta A entidade conta
	 */	
	public void excluir(Conta conta);
	
	/**
	 * Carrega conta.
	 * @param conta Id da conta.
	 * @return Entidade conta
	 */
	public Conta carregar(Integer conta);

	public List<Conta> listar(Usuario usuario);
	
	/**
	 * Busca conta favorita.
	 * @param usuario A entidade usuario.
	 * 
	 * @return conta Conta favorita
	 */
	public Conta buscarFavorita(Usuario usuario);

}
